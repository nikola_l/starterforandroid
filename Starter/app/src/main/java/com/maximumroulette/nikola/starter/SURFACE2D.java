package com.maximumroulette.nikola.starter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Created by nikola on 11/1/16.
 */

public  class SURFACE2D extends View {


    Paint paint;
    Paint paint2;
    int x=80;
    Double y=0.01;
    Double y2=0.01;
    int radius=40;
    Point ObjectPoint;


        private void init() {

            paint=new Paint();
            paint.setColor(Color.parseColor("#CD5C5C"));
            paint2=new Paint();
            paint2.setColor(Color.parseColor("#CD5C5C"));
            paint2.setAlpha(133);
            ObjectPoint = new Point(0,0);
            ObjectPoint.set(200,200);

        }

        public SURFACE2D(Context context) {
            super(context);
            init();



        }





        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);


            int X1 =(int) (Math.sin(y * (Math.PI / 180)) * 100 + 210);
            int Y1 =(int) (Math.cos(y * (Math.PI / 180)) * 100 + 210);
            System.out.print(":::" + X1);
            ObjectPoint.set( X1 , Y1  );

            y = y + 4;

            int X2 =(int) (Math.sin(y2 * (Math.PI / 180)) * 100 + 210);
            int Y2 =(int) (Math.cos(y2 * (Math.PI / 180)) * 100 + 210);

            y2 = y2 + 3;
            canvas.drawCircle(ObjectPoint.x,ObjectPoint.y, radius, paint);
            canvas.drawCircle(X2,Y2, radius, paint2);


        }


        public void update()
        {
            invalidate();
        }

    }