package com.maximumroulette.nikola.starter;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;//setOnClickListener
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

//????????

public class TwoPlayersApp extends AppCompatActivity {
    
    public static String CURRENT_RADIO = "";

    public static MediaPlayer mPlayer;
    public static MediaPlayer mPlayer2;
    public static Button PLAY_BTN;
    public boolean status_fade = true;

    float delta = 0.03f;
    float rot;
    int NOT_FIRSTTIME = 0;

    String PREPARED_STEAM1 = "NO";
    String PREPARED_STEAM2 = "NO";

    String urlRadio1 = "http://radio.nessradio.net:8000/nessradio.mp3";
    String urlRadio2 = "http://178.32.204.100:7636/listen.pls?sid=1";

    public static Button SWITCH_RADIO;

    public String TEXTFROMNET1 = "";
    public String TEXTFROMNET2 = "";
    public NotificationCompat.MessagingStyle.Message messagePASS;

    ViewGroup VEIW_MAIN;
    FrameLayout ABOUT;
    FrameLayout BACK;
    FrameLayout luanch1;

    //BTN MENU
    Button DROP_MENU_LABEL;
    SCREEN EKRAN;
    EditText CONTACT_NAME;
    EditText CONTACT_EMAIL;
    EditText CONTACT_PHONE;
    EditText MSG_BOX;
    Button SEND;
    TextView SWITCH_LABEL_1;
    TextView SWITCH_LABEL_2;
    Button BUTTON_BACK;
    Runnable runnableRot;
    Handler handlerRot;
    Runnable runnableRot2;
    Handler handlerRot2;
    Handler handlerXML;
    Runnable runnableXML;

    String MENU_STATUS;
    String MENU_CURRENT;

    AlertDialog.Builder quitAppAlert;

    TextView volumeMinus;
    TextView volumePlus;

    ImageView centerImage;
    ImageView centerImage2;
    float ALFA_1;
    float ALFA_2;
    TextView MidText;
    public static TextView BufferText;
    public SeekBar VOLUME;
    String XML_RADIO_STATUS;
    int YPOS_;
    int interval;
    float HEIGHT_MENU = 10;
    float HEIGHT_MENU_MAX = 50;
    float SIZE_OF_SCHEDULE_AUTO_GEN = 18;
    String StatusAppLoad;
    String systemInfo = "";

    public void UPDATE_WIDGET_() {

        Intent intent = new Intent(this, widgetclass.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), widgetclass.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);

    }

    protected void Quit() {
        super.finish();
    }

    public void clearMem() {/*

     //I not sure for this !
        ActivityManager amgr = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = amgr.getRunningAppProcesses();
        if (list != null){
            for (int i = 0; i < list.size(); i++) {
                ActivityManager.RunningAppProcessInfo apinfo = list.get(i);

                String[] pkgList = apinfo.pkgList;
                if ((! apinfo.processName.startsWith("com.sec")) && ((apinfo.importance > 150) || (apinfo.processName.contains("google")))) {
                    for (int j = 0; j < pkgList.length; j++) {
                        amgr.killBackgroundProcesses(pkgList[j]);
                    }
                }
            }
        }*/
    }

    //keyboard
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.FLAG_EDITOR_ACTION:

                return true;
            case KeyEvent.KEYCODE_F:

                return true;

            default:
                return super.onKeyUp(keyCode, event);
        }

    }

    Context context_pass;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {

                System.out.print("Main Activity is not the root.Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }

        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0 & getIntent().getExtras() == null) {

            System.out.print("Main Activity is not the root.Finishing Main Activity instead of launching.");
            finish();
            return;
        }

        context_pass = getApplicationContext();

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_two_players_app);

        ConnectivityManager cm = (ConnectivityManager) context_pass.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                Toast.makeText(context_pass, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                Toast.makeText(context_pass, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            }
        } else {
            // not connected to the internet
            AlertDialog.Builder dlgAlertNOINTERNET = new AlertDialog.Builder(this);
            dlgAlertNOINTERNET.setMessage("No internet access .Please enable wifi.");
            dlgAlertNOINTERNET.setTitle("Starter application say : Alert");
            dlgAlertNOINTERNET.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dismiss the dialog
                        }
                    });

            dlgAlertNOINTERNET.show();
        }

        systemInfo = "Debug-infos:";
        systemInfo += "\n OS Version: " + System.getProperty("os.version") + "(" + Build.VERSION.INCREMENTAL + ")";
        systemInfo += "\n OS API Level: " + Build.VERSION.SDK_INT;
        systemInfo += "\n Device: " + Build.DEVICE;
        systemInfo += "\n Model (and Product): " + Build.MODEL + " (" + Build.PRODUCT + ")";

        System.out.print(".........application loading");

        StatusAppLoad = "YES";
        System.out.print("####################################launching.");
        if (savedInstanceState != null)
        {
            System.out.print("####################################saved state NOT NULL.");
            String DATA1 = savedInstanceState.getString("Saved_CURRENT_RADIO");

            if (DATA1 == "2") {
                SWITCH_RADIO.performClick();
            }

            PREPARED_STEAM2 = savedInstanceState.getString("........Saved_PREPARED_STEAM2");

        } else {

            System.out.print(".......no saved state  ");

        }

        if (PREPARED_STEAM2 == "NO" && PREPARED_STEAM1 == "NO") {

            System.out.println("..........there is no prepared streams i need to create new players !");
            CREATE_PLAYERS();
        }

        YPOS_ = 1;

        // remote view WIDGET
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widgetlayout);
        remoteViews.setTextViewText(R.id.updateText, "Radio example");

        //  Intent intent = new Intent(this, Service1.class);
        //  bindService(intent, mConnection, Context.BIND_AUTO_CREATE);



        XML_RADIO_STATUS = "1";

        runnableXML = new Runnable() {
            public void run() {

                new GetData().execute();
                new GetData2().execute();

            }
        };

        handlerXML = new Handler();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ASI.execute(runnableXML);

            }
        });

        interval = 1;
        HEIGHT_MENU = 10;
        CURRENT_RADIO = "1";
        MENU_STATUS = "close";
        MENU_CURRENT = "";
        EKRAN = new SCREEN(context_pass);
        VEIW_MAIN = (ViewGroup) findViewById(R.id.activity_for_twoPlayers);
        BACK = (FrameLayout) findViewById(R.id.BACK);

/*
        AlertDialog.Builder DEBUG_LOG = new AlertDialog.Builder(this);

        DEBUG_LOG.setMessage(systemInfo);
        String forPrint = String.valueOf( EKRAN.dm.densityDpi );
        DEBUG_LOG.setTitle ( forPrint );

        DEBUG_LOG.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                    }
                });

        DEBUG_LOG.show();
*/

        /////////////

        quitAppAlert = new AlertDialog.Builder(this);

        quitAppAlert.setMessage("Quit Radio App?");
        quitAppAlert.setTitle("Starter application");

        quitAppAlert.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                        finish();
                        // MainActivity.exitApplication();

                    }
                });

        quitAppAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //dismiss the dialog


            }
        });



        /*
        BigText = new TextView(this);
        BigText.setX( 0 );
        BigText.setY(EKRAN.HEIGHT() * 0.49f );
        BigText.setText("Radio 1");
        BigText.setTypeface(null, Typeface.BOLD);
        BACK.addView(BigText);
        BigText.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
        */




        //##############################################
        //PLAYER MEDIA STAFF
        PLAY_BTN = new Button(this);
        PLAY_BTN.setText("");
        PLAY_BTN.setBackgroundResource(R.drawable.play);
        PLAY_BTN.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        PLAY_BTN.setY(EKRAN.HEIGHT() * 0.2f);
        PLAY_BTN.setX(EKRAN.WIDTH() * 0.375f);

        LayoutParams paraPlay = PLAY_BTN.getLayoutParams();
        Double d1 = (EKRAN.WIDTH() * 0.25);
        paraPlay.width = d1.intValue();
        paraPlay.height = d1.intValue();
        PLAY_BTN.setLayoutParams(paraPlay);

        //Event
        PLAY_BTN.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        if (ISITOKFORCLICK()) {

                            if (CURRENT_RADIO == "1") {

                                if (mPlayer.isPlaying() == true) {
                                    mPlayer.pause();
                                    PLAY_BTN.setBackgroundResource(R.drawable.play);
                                    BufferText.setText("Stream - Paused");

                                } else {


                                    if (PREPARED_STEAM1 == "YES") {
                                        mPlayer.start();
                                    } else {

                                        GET_CONN();

                                    }
                                    // mPlayer.setLooping(true);
                                    PLAY_BTN.setBackgroundResource(R.drawable.stop);
                                    BufferText.setText(TEXTFROMNET1);
                                }

                            } else {

                                if (mPlayer2.isPlaying() == true) {
                                    mPlayer2.pause();
                                    PLAY_BTN.setBackgroundResource(R.drawable.play);
                                    BufferText.setText("Stream - Paused");
                                } else {

                                    if (PREPARED_STEAM2 == "YES") {
                                        mPlayer2.start();
                                    } else {

                                        GET_CONN2();

                                    }
                                    PLAY_BTN.setBackgroundResource(R.drawable.stop);
                                    // BufferText.setText(TEXTFROMNET2);
                                }
                            }

                            UPDATE_WIDGET_();
                            //##############################
                            // NOTIFY SERVICES
                            Intent serviceIntent = new Intent(TwoPlayersApp.this, ServiceTwoPlayers.class);
                            //serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                            serviceIntent.setAction("com.maximumroulette.nikola.starter.PLAY");
                            startService(serviceIntent);

                        }
                    }
                }
        );


        handlerRot = new Handler();
        handlerRot2 = new Handler();
        rot = 0f;

        runnableRot = new Runnable() {
            public void run() {

                if (SWITCH_RADIO.getRotation() < 180) {

                    SWITCH_RADIO.setRotation(rot);
                    rot = rot + 3;

                    if (ALFA_1 >= 0f && ALFA_2 == 0) {
                        ALFA_1 = ALFA_1 - delta;
                        centerImage.setAlpha(ALFA_1);
                        handlerRot.postDelayed(runnableRot, interval);
                        return;

                    } else {

                        if (status_fade == true) {
                            ALFA_1 = 0;
                            ALFA_2 = 0.01f;
                            status_fade = false;
                        }
                    }

                    if (ALFA_2 <= 1f && ALFA_1 == 0) {
                        ALFA_2 = ALFA_2 + delta;
                        centerImage2.setAlpha(ALFA_2);
                        handlerRot.postDelayed(runnableRot, interval);
                    } else {
                        handlerRot.postDelayed(runnableRot, interval);
                    }


                } else {
                    SWITCH_RADIO.setRotation(180);
                    rot = 180;
                    ALFA_1 = 0f;
                    centerImage.setAlpha(ALFA_1);
                    ALFA_2 = 1f;
                    centerImage2.setAlpha(ALFA_2);
                    // centerImage.bringToFront();
                    UPDATE_WIDGET_();
                }
            }
        };

        runnableRot2 = new Runnable() {
            public void run() {

                if (SWITCH_RADIO.getRotation() > 3) {

                    SWITCH_RADIO.setRotation(rot);
                    rot = rot - 3;

                    if (ALFA_2 >= 0f && ALFA_1 == 0f) {
                        ALFA_2 = ALFA_2 - delta;
                        centerImage2.setAlpha(ALFA_2);
                        handlerRot2.postDelayed(runnableRot2, interval);
                        return;

                    } else {
                        if (status_fade == false) {
                            status_fade = true;
                            ALFA_1 = 0.01f;
                            ALFA_2 = 0f;
                        }
                    }

                    if (ALFA_1 <= 1f && ALFA_2 == 0f) {

                        ALFA_1 = ALFA_1 + delta;
                        centerImage.setAlpha(ALFA_1);
                        handlerRot2.postDelayed(runnableRot2, interval);
                    } else {
                        handlerRot2.postDelayed(runnableRot2, interval);
                    }


                } else {
                    SWITCH_RADIO.setRotation(0);
                    rot = 0;
                    ALFA_1 = 1f;
                    centerImage.setAlpha(ALFA_1);
                    ALFA_2 = 0f;
                    centerImage2.setAlpha(ALFA_2);
                    // centerImage.bringToFront();
                    UPDATE_WIDGET_();
                }
            }
        };

        new Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        //  luanch1.setVisibility(View.GONE);

                    }
                },
                1000);

        // NICE READY UI for getwidth()
        VEIW_MAIN.post(new Runnable() {
            @Override
            public void run() {

                if (savedInstanceState != null) {
                    System.out.print("saved state NOT NULL. SET PLAY BTN IMAGE TO STOP ");

                    if (mPlayer.isPlaying() || mPlayer2.isPlaying()) {
                        PLAY_BTN.setBackgroundResource(R.drawable.stop);
                    }

                }

                // mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK); ORI
                // mPlayer2.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);  ORI



                centerImage = (ImageView) findViewById(R.id.centerImage);

                centerImage.setY(EKRAN.HEIGHT() * 0.7f);
                centerImage2 = (ImageView) findViewById(R.id.centerImage2);
                centerImage2.setY(EKRAN.HEIGHT() * 0.7f);

                BUTTON_BACK = (Button) findViewById(R.id.BACKTOMAIN);
                // SEND_DROP
                BUTTON_BACK.setY((float) EKRAN.HEIGHT() * 0.745f);
                BUTTON_BACK.setX((float) EKRAN.WIDTH() * 0.08f);
                //Event
                BUTTON_BACK.setOnClickListener(
                        new Button.OnClickListener() {
                            public void onClick(View v) {

                                // quitAppAlert.show();

                                Intent myIntent = new Intent(  TwoPlayersApp.this,  MainActivity.class);
                                // myIntent.putExtra("key", value); //Optional parameters
                                TwoPlayersApp.this.startActivity(myIntent);

                            }
                        }
                );


                SWITCH_LABEL_1 = new TextView(context_pass);
                SWITCH_LABEL_1.setX(0.14f * EKRAN.WIDTH());
                SWITCH_LABEL_1.setY(0.075f * EKRAN.HEIGHT());
                SWITCH_LABEL_1.setText("RADIO 1");
                SWITCH_LABEL_1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
                BACK.addView(SWITCH_LABEL_1);


                SWITCH_LABEL_2 = new TextView(context_pass);
                SWITCH_LABEL_2.setX(0.656f * EKRAN.WIDTH());
                SWITCH_LABEL_2.setY(0.075f * EKRAN.HEIGHT());
                SWITCH_LABEL_2.setText("RADIO 2");
                SWITCH_LABEL_2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
                BACK.addView(SWITCH_LABEL_2);


                // CREATE BUTTON -- SWITCH
                SWITCH_RADIO = new Button(context_pass);
                SWITCH_RADIO.setText("");
                SWITCH_RADIO.setLayoutParams(new LinearLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
                SWITCH_RADIO.setBackgroundResource(R.drawable.switch_button);
                BACK.addView(SWITCH_RADIO);

                SWITCH_RADIO.setY(-0.003f * EKRAN.HEIGHT());  // 800 res is default ! no fix for 800

                LayoutParams paraSwitch = SWITCH_RADIO.getLayoutParams();
                Double d = (EKRAN.WIDTH() * 0.28);
                paraSwitch.width = d.intValue();
                paraSwitch.height = d.intValue();
                SWITCH_RADIO.setX((float) (EKRAN.WIDTH() / 2 - paraSwitch.width / 2));
                SWITCH_RADIO.setLayoutParams(paraSwitch);

                //caller
                SWITCH_LABEL_1.setClickable(true);
                SWITCH_LABEL_2.setClickable(true);

                SWITCH_LABEL_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (CURRENT_RADIO == "2") {
                            SWITCH_RADIO.performClick();
                        }
                    }
                });

                SWITCH_LABEL_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (CURRENT_RADIO == "1") {
                            SWITCH_RADIO.performClick();
                        }
                    }
                });

                //Event
                SWITCH_RADIO.setOnClickListener(
                        new Button.OnClickListener() {
                            public void onClick(View v) {

                                if (ISITOKFORCLICK()) {

                                    if (CURRENT_RADIO == "1") {

                                        //centerImage.bringToFront();
                                        int prevState = 0;
                                        if (mPlayer.isPlaying() == true) {

                                            prevState = 1;

                                        }
                                        //prevent error
                                        if (mPlayer.isPlaying() == true) {
                                            mPlayer.pause();
                                        }

                                        if (PREPARED_STEAM2 == "YES") {

                                            if (prevState == 1) {
                                                mPlayer2.start();
                                                PLAY_BTN.setBackgroundResource(R.drawable.stop);
                                            }

                                        } else {

                                            Toast.makeText(getApplicationContext(), "Please wait one second.. ", Toast.LENGTH_LONG).show();
                                            GET_CONN2();

                                        }

                                        BufferText.setText(TEXTFROMNET2);
                                        handlerRot.postDelayed(runnableRot, interval);
                                        CURRENT_RADIO = "2";
                                        SWITCH_LABEL_2.setShadowLayer(20, 0, 0, Color.LTGRAY);
                                        SWITCH_LABEL_1.setShadowLayer(0, 0, 0, Color.LTGRAY);
                                        //make volume same
                                        float volume1 = (float) (1 - (Math.log(100 - VOLUME.getProgress()) / Math.log(100)));
                                        mPlayer2.setVolume(volume1, volume1);
                                        MidText.setText("Listening radio 1");


                                    } else {

                                        //centerImage2.bringToFront();
                                        int prevState1 = 0;
                                        if (mPlayer2.isPlaying() == true) {
                                            prevState1 = 1;
                                        }
                                        //prevent error
                                        if (mPlayer2.isPlaying() == true) {
                                            mPlayer2.pause();
                                        }

                                        if (prevState1 == 1) {
                                            mPlayer.start();
                                            PLAY_BTN.setBackgroundResource(R.drawable.stop);
                                        }

                                        BufferText.setText(TEXTFROMNET1);
                                        handlerRot2.postDelayed(runnableRot2, interval);
                                        CURRENT_RADIO = "1";
                                        SWITCH_LABEL_2.setShadowLayer(0, 0, 0, Color.LTGRAY);
                                        SWITCH_LABEL_1.setShadowLayer(20, 0, 0, Color.LTGRAY);
                                        //make volume same
                                        float volume1 = (float) (1 - (Math.log(100 - VOLUME.getProgress()) / Math.log(100)));
                                        mPlayer.setVolume(volume1, volume1);
                                        MidText.setText("Listening radio 2");

                                    }
                                }
                            }
                        }
                );


                MidText = new TextView(context_pass);
                MidText.setX(EKRAN.WIDTH() * 0f);
                MidText.setY(EKRAN.HEIGHT() * 0.51f); // ori 54
                MidText.setText("Listening radio1 ");
                MidText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
                BACK.addView(MidText);



                BufferText = new TextView(context_pass);
                BufferText.setX(EKRAN.WIDTH() * 0.125f);
                BufferText.setY(EKRAN.HEIGHT() * 0.60f); //61
                BufferText.setText("Just wait a seconds...");
                BufferText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                BufferText.setHorizontallyScrolling(true);
                BufferText.setFocusableInTouchMode(true);
                BufferText.setFocusable(true);
                BufferText.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                BufferText.setSingleLine(true);
                BufferText.setMarqueeRepeatLimit(-1);

                BACK.addView(BufferText);
                BACK.addView(PLAY_BTN);

                VOLUME = (SeekBar) findViewById(R.id.VOLUME_FORPLAYERS);

                VOLUME.setX(EKRAN.WIDTH() * 0.185f);
                VOLUME.setY(EKRAN.HEIGHT() * 0.4f);
                LayoutParams paraVolumeSeek = VOLUME.getLayoutParams();
                Drawable myThumb = ContextCompat.getDrawable(context_pass, R.drawable.sliderico);
                myThumb.setBounds(new Rect(0, 0, 15, 25));
                VOLUME.setThumb(myThumb);

                ViewTreeObserver vto = VOLUME.getViewTreeObserver();
                vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        Resources res = getResources();
                        //Drawable thumb = res.getDrawable(R.drawable.sliderico);
                        //Drawable thumb = ContextCompat.getDrawable( getBaseContext() , R.drawable.sliderico);
                        Drawable thumb = ContextCompat.getDrawable(context_pass, R.drawable.sliderico);

                        int h = (int) (VOLUME.getMeasuredHeight() * 1); // 8 * 1.5 = 12
                        int w = h;
                        Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                        Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                        Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                        newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                        VOLUME.setThumb(newThumb);

                        VOLUME.getViewTreeObserver().removeOnPreDrawListener(this);

                        return true;
                    }
                });

                int VALUEOFVOLUMESEEKBAR = VOLUME.getMax();
                VALUEOFVOLUMESEEKBAR = VALUEOFVOLUMESEEKBAR / 2;
                VOLUME.setProgress(VALUEOFVOLUMESEEKBAR);
                VOLUME.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress,
                                                  boolean fromUser) {

                        int maxValue = seekBar.getMax();
                        int limit = (int) (maxValue / 100 * 6.5);
                        maxValue = maxValue - limit;

                        if (seekBar.getProgress() >= (maxValue)) {
                            seekBar.setProgress(maxValue);
                        } else if (seekBar.getProgress() <= limit) {
                            seekBar.setProgress(limit);
                        }


                        if (CURRENT_RADIO == "1") {
                            // TODO Auto-generated method stub
                            float volume1 = (float) (1 - (Math.log(100 - progress) / Math.log(100)));
                            mPlayer.setVolume(volume1, volume1);
                        } else {//radio 2 is active

                            float volume1 = (float) (1 - (Math.log(100 - progress) / Math.log(100)));
                            mPlayer2.setVolume(volume1, volume1);

                        }


                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                });

                Double d4 = EKRAN.WIDTH() * 0.63;
                paraVolumeSeek.width = d4.intValue();
                VOLUME.setLayoutParams(paraVolumeSeek);

                LayoutParams paraSWITCH_LABEL_1 = SWITCH_LABEL_1.getLayoutParams();
                Double d22 = 0.3 * EKRAN.WIDTH();
                paraSWITCH_LABEL_1.width = d22.intValue();
                paraSWITCH_LABEL_1.height = 100;
                SWITCH_LABEL_1.setLayoutParams(paraSWITCH_LABEL_1);
                SWITCH_LABEL_1.setGravity(Gravity.CENTER_HORIZONTAL);

                SWITCH_LABEL_2.setLayoutParams(paraSWITCH_LABEL_1);

                LayoutParams paraBufferText = BufferText.getLayoutParams();
                Double d26 = 0.75 * EKRAN.WIDTH();
                paraBufferText.width = d26.intValue();
                BufferText.setLayoutParams(paraBufferText);
                BufferText.setGravity(Gravity.CENTER_HORIZONTAL);

                LayoutParams paraMidText = MidText.getLayoutParams();
                Double d23 = 1.0 * EKRAN.WIDTH();
                paraMidText.width = d23.intValue();
                MidText.setLayoutParams(paraMidText);
                MidText.setGravity(Gravity.CENTER_HORIZONTAL);

               /* LayoutParams paraBigText = BigText.getLayoutParams();
                Double d24 = 1.0 *   EKRAN.WIDTH();
                paraBigText.width = d24.intValue();
                BigText.setLayoutParams(paraBigText);
                BigText.setGravity( Gravity.CENTER_HORIZONTAL);
                */

                LayoutParams paraCenterImage = centerImage.getLayoutParams();
                Double d21 = 0.4 * EKRAN.WIDTH();
                paraCenterImage.width = (int) (d21.intValue() * 0.89);
                paraCenterImage.height = d21.intValue();
                centerImage.setLayoutParams(paraCenterImage);
                centerImage.setX(EKRAN.WIDTH() * 0.325f);
                centerImage.setAlpha(1f);
                ALFA_1 = 1f;

                LayoutParams paraCenterImage2 = centerImage2.getLayoutParams();
                Double d2221 = 0.4 * EKRAN.WIDTH();
                paraCenterImage2.width = (int) ((d2221.intValue()) * 0.89);
                paraCenterImage2.height = d2221.intValue();
                centerImage2.setLayoutParams(paraCenterImage2);
                centerImage2.setX(EKRAN.WIDTH() * 0.325f);
                centerImage2.setLayoutParams(paraCenterImage2);
                ALFA_2 = 0f;
                centerImage2.setAlpha(0f);

                LayoutParams paraDROP_MENU_LABEL = BUTTON_BACK.getLayoutParams();
                paraDROP_MENU_LABEL.height = (int) (EKRAN.HEIGHT() * 0.08f);
                paraDROP_MENU_LABEL.width = (int) (EKRAN.HEIGHT() * 0.08f);
                BUTTON_BACK.setLayoutParams(paraDROP_MENU_LABEL);

                if (EKRAN.WIDTH() <= 480) {
                    FIX_FOR_480_WIDTH();
                } else if (EKRAN.WIDTH() == 540) {
                    FIX_FOR_540_WIDTH();
                } else if (EKRAN.WIDTH() == 720) {
                    FIX_FOR_720_WIDTH();
                } else if (EKRAN.WIDTH() == 1080) {
                    FIX_FOR_1080_WIDTH();
                } else if (EKRAN.WIDTH() == 1440) {
                    FIX_FOR_1440_WIDTH();
                }

                // 800 is default
                System.out.print(".....RESOLUTION:");
                System.out.print(String.valueOf(EKRAN.WIDTH()));

            }

        });


    }

    private void setListeners(RemoteViews contentNotify) {

        Intent radio=new Intent(context_pass, MainActivity.class);
        radio.putExtra("AN_ACTION", "do");//if necessary

        PendingIntent pRadio = PendingIntent.getActivity(context_pass, 0, radio, 0);
        //R.id.radio is a button from the layout which is created at step 2  view.setOnClickPendingIntent(R.id.radio, pRadio);

        //Follows exactly my code!
        Intent volume=new Intent(context_pass, MainActivity.class);
        volume.putExtra("DO", "volume");

        //HERE is the whole trick. Look at pVolume. I used 1 instead of 0.
        PendingIntent pVolume = PendingIntent.getActivity(context_pass, 1, volume, 0);
        contentNotify.setOnClickPendingIntent(R.id.PLAY_IN_NOTYFY, pVolume);

    }


// end of onload

    private class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            String result = "";
            //System.out.println("get text from www");
            try {
                URL url = new URL("http://maximumroulette.com/.txt");
                urlConnection = (HttpURLConnection) url.openConnection();

                int code = urlConnection.getResponseCode();

                if (code == 200) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";
                        String IsFirst = "y";

                        while ((line = bufferedReader.readLine()) != null) {
                            if (IsFirst.equals("n")) {
                                result += "   -   ";
                            }
                            result += line;
                            IsFirst = "n";
                        }

                    }
                    in.close();

                    //##############################
                    // NOTIFY SERVICES
                    //##############################
                    Intent serviceIntent = new Intent(TwoPlayersApp.this, ServiceTwoPlayers.class);
                    //serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                    startService(serviceIntent);


                }

                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            //System.out.println("Get string from internet");
            //System.out.println(result);
            TEXTFROMNET1 = result;
            BufferText.setText(TEXTFROMNET1);
            super.onPostExecute(result);
        }
    }

    private class GetData2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            String result = "";
            String IsFirst = "y";

            try {
                URL url = new URL("http://maximumroulette.com/.txt");
                urlConnection = (HttpURLConnection) url.openConnection();

                int code = urlConnection.getResponseCode();

                if (code == 200) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";

                        while ((line = bufferedReader.readLine()) != null) {
                            if (IsFirst.equals("n")) {
                                result += "   -   ";
                            }
                            result += line;
                            IsFirst = "n";
                        }

                    }
                    in.close();
                }

                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            System.out.println(result);
            TEXTFROMNET2 = result;
            super.onPostExecute(result);
        }
    }


    private class ASI extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {

            //  GET_PLAYLIST for example

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            // Call your web service here
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            // Update your UI here
            return;
        }
    }


    @Override
    protected void onResume() {
        super.onStart();
        //Do not include this check in the splash screen Activity
        System.out.println("..... on resume event");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    @Override
    protected void onStart() {
        super.onStart();

        //Do not include this check in the splash screen Activity
        System.out.println("........on start");

        if (mPlayer != null) {

            System.out.println("Player is not null");

        } else {

            System.out.println(".......Player is NULL");
            System.out.println(CURRENT_RADIO);
            System.out.println(".......create new player");


            if (CURRENT_RADIO == "2") {

                NOT_FIRSTTIME = 1;

            }
            CREATE_PLAYERS();

        }

        if (StatusAppLoad.equals("YES")) {
            System.out.println("APP ON START - SAY - App loaded from  - status");
        } else {
            System.out.println("APP ON START - SAY - App not loaded - status !@!!!!!!!!!!!! ");
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        System.out.println("APP ON STOP");
        StatusAppLoad = "NO";
        NOT_FIRSTTIME = 0;

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Do not include this check in the splash screen Activity
        System.out.println("....app start event");

    }


    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        // Do your thing
        Log.d(String.valueOf(NOT_FIRSTTIME), ".........hint ");

    }

    @Override
    public void onBackPressed() {

        System.out.println(" - BACK BTN");
        ABOUT.setVisibility(View.INVISIBLE);
        BACK.setVisibility(View.VISIBLE);
    }

    // STATE
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString("Saved_CURRENT_RADIO", CURRENT_RADIO);
        outState.putString("Saved_PREP", PREPARED_STEAM2);
        System.out.println("......save state");
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        System.out.println("Remember radio");

        String DATA1 = savedInstanceState.getString("Saved_CURRENT_RADIO");
        String DATA2 = savedInstanceState.getString("Saved_PREPARED_STEAM2");

        if (DATA2.equals("YES")) {
            PREPARED_STEAM2 = "YES";
        }
        if (DATA1.equals("2")) {
            SWITCH_RADIO.performClick();
        }

        super.onRestoreInstanceState(savedInstanceState);

    }

    void CREATE_PLAYERS() {

        //PLAYER MEDIA STAFF
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mPlayer2 = new MediaPlayer();
        mPlayer2.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mPlayer.setDataSource(urlRadio1);

        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mPlayer2.setDataSource(urlRadio2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mPlayer.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // Do something. For example: playButton.setEnabled(true);
                if (CURRENT_RADIO == "1") {
                    mPlayer.setVolume(0.4f, 0.4f);
                    mPlayer.start();
                    PLAY_BTN.setBackgroundResource(R.drawable.stop);

                    PREPARED_STEAM1 = "YES";
                }
                UPDATE_WIDGET_();
            }
        });

        mPlayer2.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (NOT_FIRSTTIME == 1) {
                    mPlayer2.setVolume(0.4f, 0.4f);
                    mPlayer2.start();
                    PLAY_BTN.setBackgroundResource(R.drawable.stop);

                    PREPARED_STEAM2 = "YES";
                }
                NOT_FIRSTTIME = 1;
                PREPARED_STEAM2 = "YES";
            }
        });

        // prepare
        try {
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context_pass, "I can get stream connection.", Toast.LENGTH_SHORT).show();

        }

        mPlayer2.prepareAsync();

    }


    String PASS_OR_NOT = "PASS";

 void FIX_FOR_480_WIDTH() {

        System.out.println("RESPONSE DESIGN FOR 480");
        SIZE_OF_SCHEDULE_AUTO_GEN = 12;
        HEIGHT_MENU_MAX = 60;
        SWITCH_LABEL_1.setX(0.126f * EKRAN.WIDTH());
        SWITCH_LABEL_1.setY(0.07f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_2.setY(0.07f * EKRAN.HEIGHT());
        SWITCH_LABEL_2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        MidText.setY(0.51f * EKRAN.HEIGHT());
        MidText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        BufferText.setY(0.57f * EKRAN.HEIGHT()); //58
        BufferText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        VOLUME.setY(0.4f * EKRAN.HEIGHT());
        SWITCH_RADIO.setY(0.01f * EKRAN.HEIGHT());

    }

    void FIX_FOR_540_WIDTH() {

        System.out.println("RESPONSE DESIGN FOR 540");
        HEIGHT_MENU_MAX = 60;
        VOLUME.setY(0.4f * EKRAN.HEIGHT());
        volumeMinus.setY(0.875f * EKRAN.HEIGHT());
        volumePlus.setY(0.875f * EKRAN.HEIGHT());
        volumePlus.setX(EKRAN.WIDTH() * 0.81f);
        volumeMinus.setX(EKRAN.WIDTH() * 0.01f);
        MidText.setY(0.475f * EKRAN.HEIGHT());
        BufferText.setY(0.57f * EKRAN.HEIGHT());
        SWITCH_RADIO.setY(0.01f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setY(0.076f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_2.setY(0.076f * EKRAN.HEIGHT());
        SWITCH_LABEL_2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_1.setX(0.131f * EKRAN.WIDTH());
    }

    void FIX_FOR_720_WIDTH() {

        System.out.println("RESPONSE DESIGN FOR 720");
        HEIGHT_MENU_MAX = 60;
        VOLUME.setY(0.4f * EKRAN.HEIGHT());

        MidText.setY(0.495f * EKRAN.HEIGHT());
        BufferText.setY(0.58f * EKRAN.HEIGHT());
        SWITCH_RADIO.setY(0.00f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setY(0.076f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_2.setY(0.076f * EKRAN.HEIGHT());
        SWITCH_LABEL_2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_1.setX(0.131f * EKRAN.WIDTH());
    }

    void FIX_FOR_1080_WIDTH() {

        System.out.println("RESPONSE DESIGN FOR 1080");
        VOLUME.setY(0.4f * EKRAN.HEIGHT());

        MidText.setY(0.475f * EKRAN.HEIGHT());
        BufferText.setY(0.57f * EKRAN.HEIGHT());
        SWITCH_RADIO.setY(0.01f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setY(0.075f * EKRAN.HEIGHT());
        SWITCH_LABEL_1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_2.setY(0.075f * EKRAN.HEIGHT());
        SWITCH_LABEL_2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        SWITCH_LABEL_1.setX(0.131f * EKRAN.WIDTH());
    }

    void FIX_FOR_1440_WIDTH() {

        SWITCH_LABEL_1.setX(0.12f * EKRAN.WIDTH());
        SWITCH_RADIO.setY(0.015f * EKRAN.HEIGHT());
        MidText.setY(0.47f * EKRAN.HEIGHT());
        BufferText.setY(0.565f * EKRAN.HEIGHT());
        VOLUME.setY(0.4f * EKRAN.HEIGHT());
        DROP_MENU_LABEL.setX((float) EKRAN.WIDTH() * 0.78f);

    }

    public Boolean ISITOKFORCLICK() {

      //  if (IF YOU HAVE SUB FRAMELAYOUT .getVisibility() == View.VISIBLE) {
      //      return false;
      //  } else {
            return true;
        //}
    }

    public void GET_CONN() {

        ConnectivityManager cm1 = (ConnectivityManager) context_pass.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm1.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to wifi
                // Toast.makeText(context_pass, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                try {
                    mPlayer.release();
                    mPlayer = new MediaPlayer();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                    mPlayer.setDataSource(urlRadio1);
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (SecurityException e) {
                    Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IllegalStateException e) {
                    Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mPlayer.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        // Do something. For example: playButton.setEnabled(true);
                        if (CURRENT_RADIO == "1") {
                            mPlayer.setVolume(0.4f, 0.4f);
                            mPlayer.start();
                            PLAY_BTN.setBackgroundResource(R.drawable.stop);
                            BufferText.setText(TEXTFROMNET1);
                            PREPARED_STEAM1 = "YES";
                        }
                    }
                });
                // prepare
                try {
                    mPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            // not connected to the internet
            AlertDialog.Builder dlgAlertNOINTERNET = new AlertDialog.Builder(this);
            dlgAlertNOINTERNET.setMessage("No access to the internet. Please enable wifi.");
            dlgAlertNOINTERNET.setTitle("Starter application - Alert");

            dlgAlertNOINTERNET.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dismiss the dialog
                        }
                    });

            dlgAlertNOINTERNET.show();
        }
    }

    public void GET_CONN2() {

        ConnectivityManager cm1 = (ConnectivityManager) context_pass.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm1.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to wifi
                Toast.makeText(context_pass, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                // Toast.makeText(context_pass, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                try {
                    //mPlayer2.stop();
                    mPlayer2.release();
                    mPlayer2 = new MediaPlayer();
                    mPlayer2.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer2.setDataSource(urlRadio2);
                    mPlayer2.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mPlayer2.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        if (NOT_FIRSTTIME == 1) {
                            mPlayer2.setVolume(0.4f, 0.4f);
                            mPlayer2.start();
                            PLAY_BTN.setBackgroundResource(R.drawable.stop);
                            BufferText.setText(TEXTFROMNET2);
                            PREPARED_STEAM2 = "YES";
                        }
                        NOT_FIRSTTIME = 1;
                    }
                });

                try {
                    mPlayer2.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            // not connected to the internet
            AlertDialog.Builder dlgAlertNOINTERNET = new AlertDialog.Builder(this);
            dlgAlertNOINTERNET.setMessage("No internet access . Please enable wifi.");
            dlgAlertNOINTERNET.setTitle("Starter application say : alert ");

            dlgAlertNOINTERNET.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //dismiss the dialog
                        }
                    });

            dlgAlertNOINTERNET.show();

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle data = intent.getExtras();
        System.out.println("I GO IT FROM INTENT FOR NOTIFY !!!!!!!");
        if (data != null && data.containsKey("PLAY_ACTION")) {
            // process your notification intent
        }
    }

}