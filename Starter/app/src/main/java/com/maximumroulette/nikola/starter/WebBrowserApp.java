package com.maximumroulette.nikola.starter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class WebBrowserApp extends AppCompatActivity {


    Button btnBack;
    Button btnGotoGoogle;
    Button btnLoadLocal;
    WebView web1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser_app);

        //############################################
        web1 = (WebView)findViewById(R.id.webBrowserMain);
        web1.setWebViewClient(new WebViewClient());
        //############################################


        //##############################################
        btnGotoGoogle = (Button)findViewById(R.id.buttonGotoGoogle);
        btnBack = (Button)findViewById(R.id.buttonGoBack);
        btnLoadLocal = (Button)findViewById(R.id.button_LoadLocal);
        //##############################################

        btnGotoGoogle.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        TextView TitleText = (TextView)findViewById(R.id.buttonGotoGoogle);
                        TitleText.setText("OK - google");
                       // web1.setWebViewClient(new WebViewClient());
                        web1.loadUrl( "http://google.com" );


                    }
                }
        );


        btnLoadLocal.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        TextView TitleText = (TextView)findViewById(R.id.button_LoadLocal);
                        TitleText.setText("Loaded");

                        web1.loadUrl("file:///android_res/raw/test.html");


                    }
                }
        );

        btnBack.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        TextView TitleText = (TextView)findViewById(R.id.buttonGoBack);

                        if (web1.canGoBack() == true) {

                            web1.goBack();
                            TitleText.setText("Go back OK");
                        }
                        else{

                            TitleText.setText("Cant Go back");
                        }


                    }
                }
        );

        //##############################################


        //

    }


}
