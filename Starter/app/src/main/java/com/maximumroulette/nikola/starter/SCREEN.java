package com.maximumroulette.nikola.starter;
/**
 * Created by nikola on 10/17/16.
 */
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.content.Context;
import android.util.TypedValue;

//##############################################
// SCREEN - EKRAN CLASS
//##############################################
public class SCREEN  {

    DisplayMetrics dm = new DisplayMetrics();
    Point size_ = new Point();
    int width;
    int height;

    SCREEN (Context CONTEXT_) {

        dm = CONTEXT_.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;
        height = dm.heightPixels;
        width = dm.widthPixels;

    }

    public int WIDTH() {

        return width;

    }
    public int HEIGHT(){

        return height;

    }
    public int W( int PER_ ){

        return width/100*PER_;

    }
    public int H( int PER_   ){


        return height/100*PER_;

    }

    //////////////////
    //extras
    /////////////////
    public int GET_PIX_FROM_DP ( float DP_VALUE )
    {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP_VALUE , dm );
    }

    public int GET_PIX_FROM_DP2 ( float DP_VALUE )
    {
        float res = DP_VALUE * ( dm.ydpi / 160f);
        return  (int) res;
    }



}