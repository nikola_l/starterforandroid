package com.maximumroulette.nikola.starter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class BROADCAST extends BroadcastReceiver {

    public BROADCAST() {}

   // Service1 S;

    public void onReceive(Context context, Intent intent) {


        System.out.println(".......NOTIFY BROADCAST");

        Bundle TES =   intent.getExtras();
        String actionFromNotifyOPEN  = (String) intent.getExtras().get("OPEN");
        String actionFromNotifyPLAY  = (String) intent.getExtras().get("PLAY");
        String actionFromNotifySWITCH  = (String) intent.getExtras().get("SWITCH");
        String actionFromNotifyDO  = (String) intent.getExtras().get("DO");


        if (actionFromNotifyPLAY != null) {
            Log.i("Receiver", "Broadcast received for play: " + actionFromNotifyPLAY);
            if (actionFromNotifyPLAY.equals("1")) {

                System.out.println("  NOTIFIUCATION BROADCAST SAY :  ACTION IS PLAY  ");

                if (TwoPlayersApp.mPlayer != null && TwoPlayersApp.mPlayer2 != null) {

                    if (TwoPlayersApp.CURRENT_RADIO.equals("1")) {

                        if ( TwoPlayersApp.mPlayer.isPlaying() ) {

                            TwoPlayersApp.PLAY_BTN.performClick();

                        }
                        else {

                            TwoPlayersApp.PLAY_BTN.performClick();

                        }

                    }
                    else if (TwoPlayersApp.CURRENT_RADIO.equals("2")) {

                        if ( TwoPlayersApp.mPlayer2.isPlaying() ) {

                            TwoPlayersApp.PLAY_BTN.performClick();
                        }
                        else {
                            TwoPlayersApp.PLAY_BTN.performClick();
                        }
                    }
                }
                else
                {
                  Log.i("Receiver", "no media player");
                }
            }
        }
        else {
            System.out.println("ACTION IS NULL  ");
        }

        if (actionFromNotifyOPEN != null) {
            Log.i("Receiver", "Broadcast received for actionFromNotifyOPEN: " + actionFromNotifyOPEN);
            if (actionFromNotifyOPEN.equals("2")) {

                System.out.println("BROADCAST SAY :  NOTING  ");

            }
        }
        else {
            System.out.println("  ACTION IS NULL  ");
        }

        if (actionFromNotifySWITCH != null) {
            Log.i("Receiver", "Broadcast received for actionFromNotifySWITCH: " + actionFromNotifySWITCH);
            if (actionFromNotifySWITCH.equals("3")) {
                System.out.println("  NOTIFIUCATION BROADCAST SAY :  SWITCH  ");
                TwoPlayersApp.SWITCH_RADIO.performClick();
            }
        }
        else {
            System.out.println("  ACTION IS NULL  ");
        }

    }
}
