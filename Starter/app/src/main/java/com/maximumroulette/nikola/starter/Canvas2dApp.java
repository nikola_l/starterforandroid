package com.maximumroulette.nikola.starter;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Canvas2dApp extends AppCompatActivity {


    private Handler mHandler;
    private SURFACE2D androidCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_canvas2d_app);
        androidCanvas = new SURFACE2D(this);
        setContentView( androidCanvas );

        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, 100);

    }



    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {

            androidCanvas.update();
            mHandler.postDelayed(this, 100);
        }
    };



}
