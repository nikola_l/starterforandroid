package com.maximumroulette.nikola.starter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GetXmlFromWWW extends AppCompatActivity {


    SCREEN EKRAN;
    Context context_pass;
    FrameLayout xml_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_xml_from_www);
        context_pass = getApplicationContext();
        EKRAN = new SCREEN(context_pass);
        xml_layout = (FrameLayout) findViewById(R.id.xml_layout);

        LoadFromAssets();


    }


    public void LoadFromAssets(){

       // XmlResourceParser res = getResources().getXml(RES.raw.XML);

        InputStream is = getResources().openRawResource(R.raw.xmldatalocal);
        Document doc = getDocument( is );

        NodeList descNodes = doc.getElementsByTagName("root");

       descNodes = descNodes.item(0).getChildNodes();


        for (int i = 0; i < descNodes.getLength(); i++) {

            if (descNodes.item(i).getLocalName() != null) {
                System.out.println("this is local name");
            }
            String content_ = descNodes.item(i).getTextContent();


            TextView TEXTVIEW = new TextView(this);
            int ID_FOR_DINAMIC_TEXTVIEW = 10000 +  i ;
            TEXTVIEW.setId( ID_FOR_DINAMIC_TEXTVIEW );
            TEXTVIEW.setTextColor(Color.BLACK);
            TEXTVIEW.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            TEXTVIEW.setText(content_);
            TEXTVIEW.setX(EKRAN.WIDTH() / 100 * 9);
            TEXTVIEW.setY(EKRAN.HEIGHT() / 10 + EKRAN.HEIGHT() / 100 * 4 * i);
            xml_layout.addView(TEXTVIEW);

            Toast.makeText(context_pass,  "FUCK" , Toast.LENGTH_SHORT).show();

            System.out.println(content_);

        }


    }

    public void GET_PLAYLIST() throws IOException {


        URL url = new URL("https://maximumroulette.com/radio/playlist.xml");
        URLConnection connection = url.openConnection();

        Document doc = getDocument(connection.getInputStream());
        NodeList descNodes = doc.getElementsByTagName("radio");

          descNodes = descNodes.item(0).getChildNodes();
        for (int i = 0; i < descNodes.getLength(); i++) {

            if (descNodes.item(i).getLocalName() != null) {
                System.out.println("this is local name");
            }
            String content_ = descNodes.item(i).getTextContent();
            System.out.println(content_);


            TextView TEXTVIEW = new TextView(this);
            TEXTVIEW.setTextColor(Color.WHITE);
            TEXTVIEW.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            TEXTVIEW.setText(content_);
            TEXTVIEW.setX(EKRAN.WIDTH() / 100 * 9);
            TEXTVIEW.setY(EKRAN.HEIGHT() / 3 + EKRAN.HEIGHT() / 100 * 2 * i);
            xml_layout.addView(TEXTVIEW);


        }




        }





 // XML STAFF
    public Document getDocument(InputStream inputStream) {
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(inputStream);
            document = db.parse(inputSource);
        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage(), e);
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage(), e);
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage(), e);
            return null;
        }
        return document;
    }

    private Document parseXML(InputStream stream)
            throws Exception {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        } catch (Exception ex) {
            throw ex;
        }

        return doc;
    }





    }

