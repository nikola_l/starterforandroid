package com.maximumroulette.nikola.starter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

public class ServiceTwoPlayers extends Service {

    public static NotificationCompat.Builder mBuilder;
    public static RemoteViews contentNotify;
    public static NotificationManager mNotificationManager;


    public ServiceTwoPlayers() {
    }

    int mStartMode;       // indicates how to behave if the service is killed
    IBinder mBinder;      // interface for clients that bind
    boolean mAllowRebind; // indicates whether onRebind should be used


    public static  String PACKAGE_NAME;

    @Override
    public void onCreate() {

        // The service is being created
        //  Toast.makeText(this, "service created", Toast.LENGTH_SHORT).show();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The service is starting, due to a call to startService()

        if (intent != null) {



            if ( intent.getAction() != null ){

                //  Toast.makeText(this, "service started intent  WITH STOP BUTTON", Toast.LENGTH_SHORT).show();

                if (TwoPlayersApp.CURRENT_RADIO.equals("1")){

                    if (TwoPlayersApp.mPlayer.isPlaying()){

                        PUSH_NOT_APP();

                    }
                    else {

                        PUSH_NOT_APP2();
                    }

                }
                else{

                    if (TwoPlayersApp.mPlayer2.isPlaying()){

                        PUSH_NOT_APP();

                    }
                    else {

                        PUSH_NOT_APP2();
                    }
                }
            }
            else {
                PUSH_NOT_APP2();
            }
        }
        return mStartMode;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()
        return mAllowRebind;
    }
    @Override
    public void onRebind(Intent intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
    }
    @Override
    public void onDestroy() {

        // Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();

    }

    public static Bitmap icon ;

    public void PUSH_NOT_APP (){
        PACKAGE_NAME = getApplicationContext().getPackageName();
        contentNotify =new RemoteViews(getApplicationContext().getPackageName(), R.layout.notifyplayerlayout);

        RemoteViews contentNotifySmall =new RemoteViews(getApplicationContext().getPackageName(), R.layout.notifyplayerlayout_small);
        icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.hudheart_full);

        //NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setShowWhen(false);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        mBuilder.setSmallIcon(R.drawable.play);
        mBuilder.setContentText("Hello World!");
        mBuilder.setLargeIcon(icon);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setContent(contentNotifySmall);
        mBuilder.setCustomBigContentView(contentNotify);

        mNotificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        contentNotify.setTextViewText( R.id.notifytexttitle , TwoPlayersApp.BufferText.getText() );

        Toast.makeText(this, TwoPlayersApp.BufferText.getText() , Toast.LENGTH_SHORT).show();

        // back to app BTN WORKS
        Intent play_from_notify=new Intent( getApplicationContext() , BROADCAST.class);
        play_from_notify.putExtra("PLAY", "1");
        PendingIntent pVolume = PendingIntent.getBroadcast( this ,  0 , play_from_notify, 0);
        contentNotify.setOnClickPendingIntent(R.id.PLAY_IN_NOTYFY , pVolume);

        Intent ACTION_O = new Intent( getApplicationContext() , BROADCAST.class);
        ACTION_O.putExtra("SWITCH", "3");
        PendingIntent pendingSwitchIntent1 = PendingIntent.getBroadcast(this, 1 , ACTION_O, 0);
        contentNotify.setOnClickPendingIntent(R.id.SWITCH_NOTIFY, pendingSwitchIntent1);

        //OPEN
        Intent ACTION_S = new Intent( getApplicationContext() ,   MainActivity.class);
        ACTION_S.putExtra("OPEN", "2");
        ACTION_S.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ACTION_S.setAction(Intent.ACTION_MAIN);
        ACTION_S.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingSwitchIntent = PendingIntent.getActivity( getApplicationContext() , 0, ACTION_S, 0);
        // ACTION_S.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        contentNotify.setOnClickPendingIntent(R.id.OPEN_APP_BTN, pendingSwitchIntent);
        mNotificationManager.notify(0, mBuilder.build());
        stopSelf();

    }


    public void PUSH_NOT_APP2 (){
        PACKAGE_NAME = getApplicationContext().getPackageName();
        contentNotify =new RemoteViews(getApplicationContext().getPackageName(), R.layout.notifyplayerlayout);

        RemoteViews contentNotifySmall =new RemoteViews(getApplicationContext().getPackageName(), R.layout.notifyplayerlayout);
        icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.hudheart_full);

        //contentNotify.setImageViewResource(R.id.PLAY_IN_NOTYFY , R.drawable.stop_2x);


        //NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setShowWhen(false);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        mBuilder.setSmallIcon(R.drawable.hudheart_full);
        mBuilder.setContentText("Hello World!");
        mBuilder.setLargeIcon(icon);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setContent(contentNotifySmall);
        mBuilder.setCustomBigContentView(contentNotify);

        mNotificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

        contentNotify.setTextViewText( R.id.notifytexttitle , TwoPlayersApp.BufferText.getText() );
        Toast.makeText(this, TwoPlayersApp.BufferText.getText() , Toast.LENGTH_SHORT).show();
        // back to app BTN WORKS
        Intent play_from_notify=new Intent( getApplicationContext() , BROADCAST.class);
        play_from_notify.putExtra("PLAY", "1");
        PendingIntent pVolume = PendingIntent.getBroadcast( this ,  0 , play_from_notify, 0);
        contentNotify.setOnClickPendingIntent(R.id.PLAY_IN_NOTYFY , pVolume);

        Intent ACTION_O = new Intent( getApplicationContext() , BROADCAST.class);
        ACTION_O.putExtra("SWITCH", "3");
        PendingIntent pendingSwitchIntent1 = PendingIntent.getBroadcast(this, 1 , ACTION_O, 0);
        contentNotify.setOnClickPendingIntent(R.id.SWITCH_NOTIFY, pendingSwitchIntent1);

        //OPEN
        Intent ACTION_S = new Intent( getApplicationContext() ,   MainActivity.class);
        ACTION_S.putExtra("OPEN", "2");
        ACTION_S.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ACTION_S.setAction(Intent.ACTION_MAIN);
        ACTION_S.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendingSwitchIntent = PendingIntent.getActivity( getApplicationContext() , 0, ACTION_S, 0);


        contentNotify.setOnClickPendingIntent(R.id.OPEN_APP_BTN, pendingSwitchIntent);

        contentNotifySmall.setOnClickPendingIntent(R.id.updateText ,pendingSwitchIntent);
        contentNotifySmall.setOnClickPendingIntent(R.id.description ,pendingSwitchIntent);

        mNotificationManager.notify(0, mBuilder.build());


        stopSelf();

    }


    private void removeNotification(String NOTIFICATION_ID_) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        notificationManager.cancel(Integer.decode(NOTIFICATION_ID_));
    }



}
