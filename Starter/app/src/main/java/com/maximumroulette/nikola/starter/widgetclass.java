package com.maximumroulette.nikola.starter;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.style.UpdateLayout;
import android.view.View;
import android.widget.RemoteViews;

import com.maximumroulette.nikola.starter.MainActivity;
import com.maximumroulette.nikola.starter.R;
import com.maximumroulette.nikola.starter.TwoPlayersApp;

import java.util.EmptyStackException;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by nikola on 12/27/16.
 */

public class widgetclass extends AppWidgetProvider {

    private static final String ACTION_CLICK = "ACTION_CLICK";
    public static String WIDGET_BUTTON = "com.maximumroulette.nikola.starter.WIDGETPLAY";
    public static String WIDGET_BUTTON_SWITCH = "com.maximumroulette.nikola.starter.SWITCH";

    public TwoPlayersApp TwoPlayersAppInstance;
    public  RemoteViews remoteViews;
    String currentRadio_INDEX = "";


    public void WID_PLAY_KLIK (){
        System.out.print(" KLIK !!!!!!!!!!!!!! " );
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        System.out.print(" widget update !!! " );
        // Get all ids
        ComponentName thisWidget = new ComponentName(context,widgetclass.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {

            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widgetlayout);

            Intent intentPLAYBTN = new Intent(WIDGET_BUTTON);
            PendingIntent pendingIntentPLAY_BTN = PendingIntent.getBroadcast(context, 0, intentPLAYBTN, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.WIDGETPLAY, pendingIntentPLAY_BTN );

            Intent intentSwitch = new Intent(WIDGET_BUTTON_SWITCH);
            PendingIntent pendingIntentS = PendingIntent.getBroadcast(context, 1, intentSwitch, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.SWITCH, pendingIntentS );




                if (TwoPlayersApp.CURRENT_RADIO != null && TwoPlayersApp.mPlayer != null   ) {
                    if (TwoPlayersApp.mPlayer.isPlaying() == true || TwoPlayersApp.mPlayer2.isPlaying() == true) {

                        currentRadio_INDEX = " Playing mp3 stream " + ((TwoPlayersApp) TwoPlayersAppInstance).CURRENT_RADIO;
                        int icon = context.getResources().getIdentifier("stop_2x", "drawable", context.getPackageName());
                        remoteViews.setInt(R.id.WIDGETPLAY, "setBackgroundResource", icon);

                    } else {

                        currentRadio_INDEX = " Paused mp3 stream " + ((TwoPlayersApp) TwoPlayersAppInstance).CURRENT_RADIO;
                        int icon = context.getResources().getIdentifier("play_btn_2x", "drawable", context.getPackageName());
                        remoteViews.setInt(R.id.WIDGETPLAY, "setBackgroundResource", icon);

                    }

                    remoteViews.setTextViewText(R.id.updateText, currentRadio_INDEX);

                }

               appWidgetManager.updateAppWidget(widgetId, remoteViews);

        }
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {

        System.out.print("WIDGET event - onReceive");

        if (WIDGET_BUTTON.equals(intent.getAction())) {

             RemoteViews remoteViews1 = new RemoteViews(context.getPackageName(), R.layout.widgetlayout);
             TwoPlayersApp.PLAY_BTN.performClick();

             ComponentName componentName = new ComponentName(context, widgetclass.class);
             AppWidgetManager.getInstance(context).updateAppWidget(componentName, remoteViews1);

        }
        else if (WIDGET_BUTTON_SWITCH.equals(intent.getAction()))
        {

            RemoteViews remoteViews1 = new RemoteViews(context.getPackageName(), R.layout.widgetlayout);
            TwoPlayersApp.SWITCH_RADIO.performClick();

            ComponentName componentName = new ComponentName(context, widgetclass.class);
            AppWidgetManager.getInstance(context).updateAppWidget(componentName, remoteViews1);

        }
        else {

            super.onReceive(context, intent);

        }

    }

}