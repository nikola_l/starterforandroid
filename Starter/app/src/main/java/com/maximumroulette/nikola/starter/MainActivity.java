package com.maximumroulette.nikola.starter;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import  android.widget.LinearLayout;
import  android.view.ViewGroup;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    /*
    *
    *
    * licence for images used in this project


      - background "Designed by Kjpargeter / Freepik"




    * */

    public String deviceInfo = "NoDataForNow";
    Context context_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);


        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {

                System.out.print("Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }

        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0 & getIntent().getExtras() == null) {

            System.out.print("Main Activity is not the root.  Finishing Main Activity instead of launching.");
            finish();
            return;
        }


        context_pass = getApplicationContext();
        setContentView(R.layout.activity_main);

        //##############################################
        Context context_pass = getApplicationContext();
        //##############################################

        // CUSTOM CLASS FROM PROJECT
        //##############################################
        SCREEN EKRAN = new SCREEN( context_pass );
        //##############################################

        //##############################################
        ViewGroup VEIW_MAIN = (ViewGroup) findViewById(R.id.activity_main);
        //##############################################



        //Device info
        deviceInfo ="Debug-infos:";
        deviceInfo += "\n OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
        deviceInfo += "\n OS API Level: " + android.os.Build.VERSION.SDK_INT;
        deviceInfo += "\n Device: " + android.os.Build.DEVICE;
        deviceInfo += "\n Model (and Product): " + android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")";


        /*
        //##############################################
        // CREATE BUTTON BEST WAY
        //##############################################
        */

        final Button gotoPlayerStreamApp = new Button(this);
        gotoPlayerStreamApp.setText( "Media Player" );
        VEIW_MAIN.addView(gotoPlayerStreamApp);
        gotoPlayerStreamApp.setY((float) (EKRAN.HEIGHT()*0.1));
        gotoPlayerStreamApp.setX( (float) 0 );
        gotoPlayerStreamApp.setWidth( (int)  (EKRAN.WIDTH()*0.5) );

        gotoPlayerStreamApp.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  PlayerApp.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );

        final Button gotoWebBrowser = new Button(this);
        gotoWebBrowser.setText( "Web Browser" );
        VEIW_MAIN.addView(gotoWebBrowser);
        gotoWebBrowser.setY( (float) (EKRAN.HEIGHT()*0.15));
        gotoWebBrowser.setX( (float) 0 );
        gotoWebBrowser.setWidth( (int) (EKRAN.WIDTH()*0.5));
        gotoWebBrowser.setHeight( (int) (EKRAN.HEIGHT()*0.05));

        gotoWebBrowser.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  WebBrowserApp.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );


        final Button gotoCanvasApp = new Button(this);
        gotoCanvasApp.setText( "Canvas system" );
        VEIW_MAIN.addView(gotoCanvasApp);
        gotoCanvasApp.setY( (float)  (EKRAN.HEIGHT()*0.2));
        gotoCanvasApp.setX((float)  0 );
        gotoCanvasApp.setWidth((int) (EKRAN.WIDTH()*0.5));

        gotoCanvasApp.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  Canvas2dApp.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );
        ////////////////



        final Button gotoTwoPlayer = new Button(this);
        gotoTwoPlayer.setText( "Service - Two stream" );
        VEIW_MAIN.addView(gotoTwoPlayer);
        gotoTwoPlayer.setY( (float)  (EKRAN.HEIGHT()*0.25));
        gotoTwoPlayer.setX((float)  0 );
        gotoTwoPlayer.setWidth((int) (EKRAN.WIDTH()*0.5));

        gotoTwoPlayer.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  TwoPlayersApp.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );
        ////////////////

        final Button gotoGetXML = new Button(this);
        gotoGetXML.setText( "GET XML - from url" );
        VEIW_MAIN.addView(gotoGetXML);
        gotoGetXML.setY( (float)  (EKRAN.HEIGHT()*0.3));
        gotoGetXML.setX((float)  0 );
        gotoGetXML.setWidth((int) (EKRAN.WIDTH()*0.5));

        gotoGetXML.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  GetXmlFromWWW.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );
        ////////////////

        final Button gotoEMAIL = new Button(this);
        gotoEMAIL.setText( "EMAIL FORM" );
        VEIW_MAIN.addView(gotoEMAIL);
        gotoEMAIL.setY( (float)  (EKRAN.HEIGHT()*0.35));
        gotoEMAIL.setX((float)  0 );
        gotoEMAIL.setWidth((int) (EKRAN.WIDTH()*0.5));

        gotoEMAIL.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent myIntent = new Intent(  MainActivity.this,  SendEmailFrom.class);
                        // myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                    }
                }
        );
        ////////////////






        System.out.print("Activity loaded success.");
    }
    ////////////////////////
    //end of activity load
    ////////////////////////


    public void UPDATE_WIDGET_ () {

       /* Intent intent = new Intent(this, widgetclass.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), widgetclass.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
       */

    }
    //##############################################


}
