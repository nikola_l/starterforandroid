package com.maximumroulette.nikola.starter;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailFrom extends AppCompatActivity {

    Button SEND;
    TextView CONTACT_EMAIL;
    TextView CONTACT_NAME;
    TextView CONTACT_PHONE;
    TextView MSG_BOX;
    Context context_pass;
    MimeMessage messagePASS;
    String PASS_OR_NOT;
    FrameLayout VIEW;
    SCREEN EKRAN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email_from);

        context_pass = getApplicationContext();
        EKRAN = new SCREEN(context_pass);

        SEND = new Button(this);

        CONTACT_EMAIL = new EditText(this);
        CONTACT_NAME = new EditText(this);
        CONTACT_PHONE = new EditText(this);

        CONTACT_PHONE.setInputType( InputType.TYPE_CLASS_PHONE );
        CONTACT_EMAIL.setInputType( InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        MSG_BOX = new EditText(this);
        PASS_OR_NOT = "NO";

        VIEW = (FrameLayout) findViewById(R.id.EmailForm);
        VIEW.addView(MSG_BOX);
        VIEW.addView(CONTACT_PHONE);
        VIEW.addView(CONTACT_NAME);
        VIEW.addView(CONTACT_EMAIL);
        VIEW.addView(SEND);



        CONTACT_NAME.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CONTACT_NAME.setTextColor(Color.WHITE);
            }
        });

        CONTACT_EMAIL.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CONTACT_EMAIL.setTextColor(Color.WHITE);
            }
        });

        CONTACT_PHONE.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CONTACT_PHONE.setTextColor(Color.WHITE);
            }
        });



        SEND.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        String email = CONTACT_EMAIL.getText().toString().trim();
                        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


                        if (!CONTACT_NAME.getText().toString().isEmpty() & !CONTACT_EMAIL.getText().toString().isEmpty() & !MSG_BOX.getText().toString().isEmpty()) {


                            if (email.matches(emailPattern)) {
                                Toast.makeText(getApplicationContext(), "Sending email ...", Toast.LENGTH_SHORT).show();

                                SEND_MSG();

                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                                CONTACT_EMAIL.setTextColor(Color.RED);
                            }


                        } else {

                            if (CONTACT_NAME.getText().toString().isEmpty()) {
                                Toast.makeText(context_pass, "Please enter your name!", Toast.LENGTH_SHORT).show();
                            }

                            if (CONTACT_EMAIL.getText().toString().isEmpty()) {
                                Toast.makeText(context_pass, "Please enter your email address!", Toast.LENGTH_SHORT).show();
                            }

                            if (MSG_BOX.getText().toString().isEmpty()) {
                                Toast.makeText(context_pass, "Please enter your message!", Toast.LENGTH_SHORT).show();
                            }


                        }


                    }

                });


        Toast.makeText(context_pass, "Load", Toast.LENGTH_SHORT).show();


        VIEW.post(new Runnable() {
            @Override
            public void run() {


                SEND.setY(EKRAN.HEIGHT() * 0.7f);
                SEND.setX(EKRAN.WIDTH() * 0.1f);
                SEND.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                SEND.setBackgroundResource(R.drawable.hudheart_full);
                SEND.setText("SEND MSG");

                CONTACT_NAME.setY(EKRAN.HEIGHT() * 0.20f);
                CONTACT_NAME.setX(EKRAN.WIDTH() * 0.1f);
                CONTACT_NAME.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                CONTACT_NAME.setText("Nikola");

                CONTACT_EMAIL.setY(EKRAN.HEIGHT() * 0.28f);
                CONTACT_EMAIL.setX(EKRAN.WIDTH() * 0.1f);
                CONTACT_EMAIL.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                CONTACT_PHONE.setY(EKRAN.HEIGHT() * 0.36f);
                CONTACT_PHONE.setX(EKRAN.WIDTH() * 0.1f);
                CONTACT_PHONE.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                MSG_BOX.setY(EKRAN.HEIGHT() * 0.44f);
                MSG_BOX.setX(EKRAN.WIDTH() * 0.1f);
                MSG_BOX.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                MSG_BOX.setBackgroundColor(Color.WHITE);

                LayoutParams ParaCONTACT_NAME = CONTACT_NAME.getLayoutParams();
                ParaCONTACT_NAME.height = (int) (EKRAN.HEIGHT() * 0.07f);
                ParaCONTACT_NAME.width = (int) (EKRAN.WIDTH() * 0.8f);
                CONTACT_NAME.setLayoutParams(ParaCONTACT_NAME);

                LayoutParams paraCONTACT_PHONE = CONTACT_PHONE.getLayoutParams();
                paraCONTACT_PHONE.height = (int) (EKRAN.HEIGHT() * 0.07f);
                paraCONTACT_PHONE.width = (int) (EKRAN.WIDTH() * 0.8f);
                CONTACT_PHONE.setLayoutParams(paraCONTACT_PHONE);

                LayoutParams paraCONTACT_EMAIL = CONTACT_EMAIL.getLayoutParams();
                paraCONTACT_EMAIL.height = (int) (EKRAN.HEIGHT() * 0.07f);
                paraCONTACT_EMAIL.width = (int) (EKRAN.WIDTH() * 0.8f);
                CONTACT_EMAIL.setLayoutParams(paraCONTACT_EMAIL);

                LayoutParams paraMSG_BOX = MSG_BOX.getLayoutParams();
                paraMSG_BOX.height = (int) (EKRAN.HEIGHT() * 0.25f);
                paraMSG_BOX.width = (int) (EKRAN.WIDTH() * 0.8f);
                MSG_BOX.setLayoutParams(paraMSG_BOX);

                LayoutParams paraSEND = SEND.getLayoutParams();
                paraSEND.height = (int) (EKRAN.HEIGHT() * 0.07f);
                paraSEND.width = (int) (EKRAN.WIDTH() * 0.8f);
                SEND.setLayoutParams(paraSEND);


                Toast.makeText(context_pass, "ASETUP EMAIL COORDINATE", Toast.LENGTH_SHORT).show();




            }
            });

    }

    void SEND_MSG() {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("zlatnaspirala@gmail.com", "123456");
            }
        });

        String BODY = "<h1>Email message from :</h1> " + CONTACT_EMAIL.getText() + " .<br/>    Sender name : " + CONTACT_NAME.getText() + " <br/>. Sender phone : " + CONTACT_PHONE.getText() + " .<br/> <br/> Message : " + MSG_BOX.getText();

        try {

            messagePASS = new MimeMessage(session);
            messagePASS.setFrom(new InternetAddress("zlatnaspirala@gmail.com"));
            messagePASS.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse("zlatnaspirala@gmail.com"));
            messagePASS.setSubject("Message from radio android app user");
            messagePASS.setContent(BODY, "text/html; charset=utf-8");

            final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);


            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    // Log.d(TAG, "onPreExecute()");
                }

                @Override
                protected Void doInBackground(Void... params) {

                    try {


                        Transport.send(messagePASS);
                        PASS_OR_NOT = "YES";

                    } catch (MessagingException e) {

                        PASS_OR_NOT = "NO";
                        e.printStackTrace();

                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void res) {
                    // System.(TAG, "onPostExecute()");

                    if (PASS_OR_NOT.equals("YES")) {
                        dlgAlert.setMessage("Thanks for contacting us!\n" +
                                "We will get back to you as soon as possible.");
                        dlgAlert.setTitle("ThaiVisaRadio app");

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //dismiss the dialog
                                    }
                                });

                        dlgAlert.show();
                    } else {

                        dlgAlert.setMessage("message has not been sent successfully");
                        dlgAlert.setTitle("Error sending email");

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //dismiss the dialog
                                    }
                                });

                        dlgAlert.show();


                    }


                }
            }.execute();

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }


    }


}
